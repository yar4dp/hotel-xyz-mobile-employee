package id.faizal.hotelxyzemployee.feature.reservation.list

import android.content.Context
import android.graphics.Color
import android.support.transition.AutoTransition
import android.support.transition.TransitionManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.feature.reservation.approvaldialog.ApprovalDialog
import id.faizal.hotelxyzemployee.model.ApprovalStatus
import id.faizal.hotelxyzemployee.model.ReservationModel
import id.faizal.hotelxyzemployee.utilities.formatCurrency

class ReservationListAdapter(
        val context: Context,
        val list: List<ReservationModel>,
        val callback: ReservationListAdapter.RefreshReservationListAdapter
) : RecyclerView.Adapter<ReservationListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_reservation, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.showReservationItem(list[position])
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        var item: ReservationModel? = null

        fun showReservationItem(item: ReservationModel) {
            this.item = item
            val rootView = itemView.findViewById<LinearLayout>(R.id.rootView)
            val guestNameText = itemView.findViewById<TextView>(R.id.guestName)
            val timeText = itemView.findViewById<TextView>(R.id.time)
            val estimatedRateText = itemView.findViewById<EditText>(R.id.inputEstimatedRate)
            val detailButton = itemView.findViewById<Button>(R.id.detailButton)
            val seeMoreLayout = itemView.findViewById<LinearLayout>(R.id.seeMoreLayout)
            val inputFirstName = itemView.findViewById<EditText>(R.id.inputFirstName)
            val inputLastName = itemView.findViewById<EditText>(R.id.inputLastName)
            val inputAddress = itemView.findViewById<EditText>(R.id.inputAddress)
            val inputPhoneNumber = itemView.findViewById<EditText>(R.id.inputPhoneNumber)
            val inputPerson = itemView.findViewById<EditText>(R.id.inputPerson)
            val inputAdditionalRequest = itemView.findViewById<TextView>(R.id.inputAdditionalRequest)
            val approvalStatusText = itemView.findViewById<TextView>(R.id.approvalStatusText)
            val inputApprovalNote = itemView.findViewById<EditText>(R.id.inputApprovalNote)
            val approvalButton = itemView.findViewById<Button>(R.id.approveButton)

            guestNameText.text = String.format(item.room.name)
            timeText.text = String.format("%s - %s", item.checkInTime(), item.checkOutTime())
            estimatedRateText.setText(formatCurrency(item.estimatedRate))
            inputFirstName.setText(item.guestFirstName)
            inputLastName.setText(item.guestLastName)
            inputAddress.setText(item.guestAddress)
            inputPhoneNumber.setText(item.guestPhoneNumber)
            inputPerson.setText(item.totalPerson)
            inputAdditionalRequest.setText(item.additionalRequest)

            Log.d("approval", Gson().toJson(item.approval))

            if (item.approval === null) {
                approvalStatusText.setText("Pending")
                approvalStatusText.setTextColor(Color.GRAY)
            } else {
                inputApprovalNote.setText(item.approval.note)
                if (item.approval.status() == ApprovalStatus.Rejected) {
                    approvalStatusText.setText("Rejected")
                    approvalStatusText.setTextColor(Color.RED)
                }
                if (item.approval.status() == ApprovalStatus.Approved) {
                    approvalStatusText.setText("Approved")
                    approvalStatusText.setTextColor(Color.GREEN)
                }
            }

            detailButton.setOnClickListener {
                if (seeMoreLayout.visibility == View.GONE) {
                    TransitionManager.beginDelayedTransition(rootView, AutoTransition());
                    seeMoreLayout.visibility = View.VISIBLE
                    if (item.approval === null) {
                        approvalButton.visibility = View.VISIBLE
                    }
                    detailButton.text = "Hide Detail"
                } else {
                    seeMoreLayout.visibility = View.GONE
                    approvalButton.visibility = View.GONE
                    detailButton.text = "See Detail"
                }
            }

            approvalButton.setOnClickListener {
                val dialog = ApprovalDialog(context, item)
                dialog.setOnDismissListener {
                    callback.refreshReservationListAdapter()
                }
                dialog.show()
            }
        }

    }

    interface RefreshReservationListAdapter {
        fun refreshReservationListAdapter()
    }

}