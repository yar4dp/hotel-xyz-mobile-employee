package id.faizal.hotelxyzemployee.feature.reservation.report

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import crl.android.pdfwriter.PDFWriter
import crl.android.pdfwriter.PaperSize
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.feature.reservation.list.ReservationListAdapter
import id.faizal.hotelxyzemployee.model.PaginationModel
import id.faizal.hotelxyzemployee.model.ReservationModel
import id.faizal.hotelxyzemployee.model.ResponseModel
import id.faizal.hotelxyzemployee.network.apiService
import id.faizal.hotelxyzemployee.utilities.Employee
import id.faizal.hotelxyzemployee.utilities.formatCurrency
import id.faizal.hotelxyzemployee.utilities.showSnackbar
import kotlinx.android.synthetic.main.activity_reservation_report.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException


class ReservationReportActivity : AppCompatActivity(), Callback<ResponseModel<PaginationModel<ReservationModel>>>, ReservationListAdapter.RefreshReservationListAdapter {

    var searchBy: String = ""
    var from: String = ""
    var to: String = ""
    var titlePage: String = ""
    var adapter: ReservationListAdapter? = null

    // Storage Permissions
    private val REQUEST_EXTERNAL_STORAGE = 1
    private val PERMISSIONS_STORAGE = arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation_report)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Report"

        val bundle = intent.extras.getBundle("bundle")
        searchBy = bundle.getString("searchBy")
        from = bundle.getString("from")
        to = bundle.getString("to")

        progressBarLayout.visibility = View.VISIBLE
        recyclerViewLayout.visibility = View.GONE

        refreshReservationListAdapter()
    }

    override fun onFailure(call: Call<ResponseModel<PaginationModel<ReservationModel>>>?, t: Throwable) {
        showSnackbar(this.window.decorView, t.localizedMessage)
    }

    override fun onResponse(call: Call<ResponseModel<PaginationModel<ReservationModel>>>?, response: Response<ResponseModel<PaginationModel<ReservationModel>>>) {
        if (response.body()?.status == "200") {
            showReservationList(response.body()?.data?.data!!)
            titlePage = response.body()?.message!! + " by " + searchBy
        } else {
            showSnackbar(this.window.decorView, response.body()?.message!!)
        }
    }

    fun showReservationList(list: List<ReservationModel>) {
        if (list.count() == 0) {
            showSnackbar(this.window.decorView, "Data tidak ditemukan.")
        }
        progressBarLayout.visibility = View.GONE
        recyclerViewLayout.visibility = View.VISIBLE
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        this.adapter = ReservationListAdapter(this, list, this)
        recyclerView.adapter = this.adapter
        recyclerView.isNestedScrollingEnabled = false
    }

    override fun refreshReservationListAdapter() {
        apiService.listByDate(Employee.token(this)!!, searchBy, from, to).enqueue(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        if (item?.itemId == R.id.action_print_pdf) {
            verifyStoragePermissions(this)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_report_list, menu)
        return true
    }

    private fun generatePDF() {
        if (adapter != null) {
            val writer = PDFWriter(PaperSize.FOLIO_WIDTH, PaperSize.FOLIO_HEIGHT)
            val fontSize = 12
            val margin = 48
            var height = PaperSize.FOLIO_HEIGHT - margin
            writer.addText(margin, height, fontSize, titlePage)
            var itemNo = 1
            var estimatedIncome = 0.0
            for (item: ReservationModel in adapter!!.list) {
                height = height - fontSize
                writer.addText(margin, height, fontSize, "")
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Reservation #" + itemNo)
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Room Name : " + item.room.name)
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Check In : " + item.checkInDate())
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Check Out : " + item.checkOutDate())
                if (item.approval != null) {
                    height = height - fontSize
                    writer.addText(margin, height, fontSize, "Approved? : " + item.approval.status().toString())
                    height = height - fontSize
                    writer.addText(margin, height, fontSize, "Approval Note : " + item.approval.note)
                } else {
                    height = height - fontSize
                    writer.addText(margin, height, fontSize, "Approved? : None")
                    height = height - fontSize
                    writer.addText(margin, height, fontSize, "Approval Note : -")
                }
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Guest Name: " + item.guestLastName)
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Guest's Address : " + item.guestAddress)
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Guest's Phone Number : " + item.guestPhoneNumber)
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Additional Request : " + item.additionalRequest)
                height = height - fontSize
                writer.addText(margin, height, fontSize, "Estimated Rate : " + formatCurrency(item.estimatedRate))
                itemNo++
                estimatedIncome += item.estimatedRate
            }
            height = height - fontSize
            writer.addText(margin, height, fontSize, "")
            height = height - fontSize
            writer.addText(margin, height, fontSize, "Total Data : " + adapter?.list?.count())
            height = height - fontSize
            writer.addText(margin, height, fontSize, "Estimated Income : " + formatCurrency(estimatedIncome))
            outputToFile("Report " + titlePage + ".pdf", writer.asString(), "ISO-8859-1");
        }
    }

    private fun outputToFile(fileName: String, pdfContent: String, encoding: String) {
        val newFile = File(Environment.getExternalStorageDirectory().absolutePath + "/" + fileName)
        try {
            newFile.createNewFile()
            try {
                val pdfFile = FileOutputStream(newFile)
                pdfFile.write(pdfContent.toByteArray(charset(encoding)))
                pdfFile.close()
                showSnackbar(this.window.decorView, "File created on " + newFile)
            } catch (e: FileNotFoundException) {
                showSnackbar(this.window.decorView, e.localizedMessage)
            }

        } catch (e: IOException) {
            showSnackbar(this.window.decorView, e.localizedMessage)
        }

    }

    fun verifyStoragePermissions(activity: Activity) {
        // Check if we have write permission
        val permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            )
        } else {
            generatePDF()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!

                    generatePDF()

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    verifyStoragePermissions(this)

                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

}
