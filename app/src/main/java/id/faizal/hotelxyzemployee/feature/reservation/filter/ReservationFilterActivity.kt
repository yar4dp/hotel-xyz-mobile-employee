package id.faizal.hotelxyzemployee.feature.reservation.filter

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.DatePicker
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.feature.reservation.report.ReservationReportActivity
import id.faizal.hotelxyzemployee.utilities.formatCalendar
import id.faizal.hotelxyzemployee.utilities.showDatePicker
import kotlinx.android.synthetic.main.activity_search_and_report.*
import java.util.*

class ReservationFilterActivity: AppCompatActivity() {

    var fromCalendar = Calendar.getInstance()
    var toCalendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_and_report)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Search Reservation By Date"

        inputFromDate.setText(formatCalendar(fromCalendar))

        toCalendar.add(Calendar.DAY_OF_MONTH, 1)
        inputToDate.setText(formatCalendar(toCalendar))

        inputFromDate.setOnClickListener {
            showDatePicker(this, fromCalendar, object: DatePickerDialog.OnDateSetListener {
                override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
                    fromCalendar.set(p1, p2, p3)
                    inputFromDate.setText(formatCalendar(fromCalendar))
                }

            })
        }

        inputToDate.setOnClickListener {
            showDatePicker(this, toCalendar, object: DatePickerDialog.OnDateSetListener {
                override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
                    toCalendar.set(p1, p2, p3)
                    inputToDate.setText(formatCalendar(toCalendar))
                }

            })
        }

        buttonSearch.setOnClickListener {
            var searchBy = ""
            if (radioReservationDate.isChecked) {
                searchBy = "created_at"
            }
            if (radioCheckOutDate.isChecked) {
                searchBy = "check_out"
            }
            if (radioCheckInDate.isChecked) {
                searchBy = "check_in"
            }

            fromCalendar.set(Calendar.HOUR, 0)
            fromCalendar.set(Calendar.MINUTE, 0)
            fromCalendar.set(Calendar.SECOND, 0)

            toCalendar.set(Calendar.HOUR, 23)
            toCalendar.set(Calendar.MINUTE, 59)
            toCalendar.set(Calendar.SECOND, 59)

            val from = formatCalendar(fromCalendar, "yyyy-MM-dd HH:mm:ss")
            val to = formatCalendar(toCalendar, "yyyy-MM-dd HH:mm:ss")

            val bundle = Bundle()
            bundle.putString("searchBy", searchBy)
            bundle.putString("from", from)
            bundle.putString("to", to)

            val intent = Intent(this, ReservationReportActivity::class.java)
            intent.putExtra("bundle", bundle)

            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}