package id.faizal.hotelxyzemployee.feature.reservation.list


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.model.PaginationModel
import id.faizal.hotelxyzemployee.model.ReservationModel
import id.faizal.hotelxyzemployee.model.ResponseModel
import id.faizal.hotelxyzemployee.network.apiService
import id.faizal.hotelxyzemployee.utilities.Employee
import id.faizal.hotelxyzemployee.utilities.showSnackbar
import kotlinx.android.synthetic.main.fragment_reservation_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ReservationListFragment : Fragment(), Callback<ResponseModel<PaginationModel<ReservationModel>>>, ReservationListAdapter.RefreshReservationListAdapter {

    var mode: Int? = -1

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_reservation_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        recyclerViewLayout.visibility = View.GONE
        super.onViewCreated(view, savedInstanceState)
        mode = arguments.getInt("mode")
        refreshReservationListAdapter()
    }

    override fun onResponse(call: Call<ResponseModel<PaginationModel<ReservationModel>>>?, response: Response<ResponseModel<PaginationModel<ReservationModel>>>) {
        if (response.body()?.status == "200") {
            showReservationList(response.body()?.data?.data!!)
        } else {
            showSnackbar(activity.window.decorView, response.body()?.message!!)
        }
    }

    override fun onFailure(call: Call<ResponseModel<PaginationModel<ReservationModel>>>?, t: Throwable) {
        showSnackbar(activity.window.decorView, t.localizedMessage)
    }

    fun showReservationList(list: List<ReservationModel>) {
        progressBarLayout.visibility = View.GONE
        recyclerViewLayout.visibility = View.VISIBLE
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = ReservationListAdapter(context, list, this)
        recyclerView.isNestedScrollingEnabled = false
    }

    override fun refreshReservationListAdapter() {
        when (mode) {
            0 -> {
                apiService.listPendingReservation(Employee.token(context)!!).enqueue(this)
            }
            1 -> {
                apiService.listApprovedReservation(Employee.token(context)!!).enqueue(this)
            }
            2 -> {
                apiService.listRejectedReservation(Employee.token(context)!!).enqueue(this)
            }
        }
    }

}