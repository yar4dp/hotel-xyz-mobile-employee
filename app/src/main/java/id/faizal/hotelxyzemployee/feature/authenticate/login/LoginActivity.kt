package id.faizal.hotelxyzemployee.feature.authenticate.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.feature.reservation.list.ReservationListActivity
import id.faizal.hotelxyzemployee.model.ResponseModel
import id.faizal.hotelxyzemployee.model.TokenModel
import id.faizal.hotelxyzemployee.network.apiService
import id.faizal.hotelxyzemployee.utilities.Employee
import id.faizal.hotelxyzemployee.utilities.printLog
import id.faizal.hotelxyzemployee.utilities.showSnackbar
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity(), Callback<ResponseModel<TokenModel>>, Employee.GetProfileInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener {
            if (loginValid()) {
                login()
            }
        }

        if (Employee.hasBeenLoggedIn(this)) {
            showDashboard()
        }
    }

    private fun loginValid() : Boolean {
        if (inputEmail.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input your email.")
            return false
        }
        if (inputPassword.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input your password.")
            return false
        }
        return true
    }

    private fun login() {
        apiService.employeeLogin(inputEmail.text.toString(), inputPassword.text.toString()).enqueue(this)
    }

    override fun onResponse(call: Call<ResponseModel<TokenModel>>?, response: Response<ResponseModel<TokenModel>>) {
        if (response.body()?.status == "200") {
            Employee.login(this, response.body()!!.data, this)
        } else {
            showSnackbar(window.decorView, response.body()!!.message)
        }
    }

    override fun onFailure(call: Call<ResponseModel<TokenModel>>?, t: Throwable) {
        showSnackbar(window.decorView, t.localizedMessage)
    }

    override fun profileSuccessRetrieved() {
        loginSuccess()
    }

    private fun loginSuccess() {
        if (intent.extras != null) {
            val previousIntent = intent.extras["previousIntent"] as Intent?
            previousIntent?.putExtra("message", "Welcome back!")
            startActivity(previousIntent)
            finish()
        } else {
            showDashboard()
        }
    }

    override fun profileFailedToRetrievedBecause(message: String) {
        showSnackbar(window.decorView, message)
    }

    private fun showDashboard() {
        startActivity(Intent(this, ReservationListActivity::class.java))
        finishAffinity()
    }

}
