package id.faizal.hotelxyzemployee.feature.splashscreen

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.feature.authenticate.login.LoginActivity
import id.faizal.hotelxyzemployee.feature.reservation.list.ReservationListActivity
import id.faizal.hotelxyzemployee.utilities.Employee

class SplashScreenActivity : AppCompatActivity(), Employee.GetProfileInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        if (Employee.hasToken(this)) {
            val token = Employee.token(this)
            Employee.refreshProfile(token!!, this)
        } else {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    override fun profileSuccessRetrieved() {
        startActivity(Intent(this, ReservationListActivity::class.java))
    }

    override fun profileFailedToRetrievedBecause(message: String) {
        startActivity(Intent(this, LoginActivity::class.java))
    }
}
