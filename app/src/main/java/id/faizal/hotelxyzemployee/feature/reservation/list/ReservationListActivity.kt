package id.faizal.hotelxyzemployee.feature.reservation.list

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.utilities.Employee
import id.faizal.hotelxyzemployee.feature.authenticate.login.LoginActivity
import kotlinx.android.synthetic.main.activity_reservation_list.*
import id.faizal.hotelxyzemployee.feature.reservation.filter.ReservationFilterActivity

class ReservationListActivity : AppCompatActivity() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation_list)

        setSupportActionBar(toolbar)
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        container.adapter = mSectionsPagerAdapter

        tabs.setupWithViewPager(container)
        tabs.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Todo
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                // Todo
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                mSectionsPagerAdapter!!.refreshFragment(tab?.position!!)
            }

        })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_reservation_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_logout) {
            Employee.logout(this)
            startActivity(Intent(this, LoginActivity::class.java))
            finishAffinity()
        }

        if (id == R.id.action_search_and_report) {
            startActivity(Intent(this, ReservationFilterActivity::class.java))
        }

        return super.onOptionsItemSelected(item)
    }


    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        var fragments = ArrayList<ReservationListFragment>()

        init {
            val fragment0 = ReservationListFragment()
            val bundle0 = Bundle()
            bundle0.putInt("mode", 0)
            fragment0.arguments = bundle0
            fragments.add(fragment0)

            val fragment1 = ReservationListFragment()
            val bundle1 = Bundle()
            bundle1.putInt("mode", 1)
            fragment1.arguments = bundle1
            fragments.add(fragment1)

            val fragment2 = ReservationListFragment()
            val bundle2 = Bundle()
            bundle2.putInt("mode", 2)
            fragment2.arguments = bundle2
            fragments.add(fragment2)
        }

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int {
            return 3
        }

        override fun getPageTitle(position: Int): CharSequence {
            when(position) {
                0 -> return "Pending"
                1 -> return "Approved"
                2 -> return "Rejected"
            }
            return ""
        }

        fun refreshFragment(position: Int) {
            fragments[position].refreshReservationListAdapter()
        }

    }

}
