package id.faizal.hotelxyzemployee.feature.reservation.approvaldialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.WindowManager
import id.faizal.hotelxyzemployee.R
import id.faizal.hotelxyzemployee.model.ReservationModel
import id.faizal.hotelxyzemployee.model.ResponseModel
import id.faizal.hotelxyzemployee.network.apiService
import id.faizal.hotelxyzemployee.utilities.Employee
import id.faizal.hotelxyzemployee.utilities.showAlertDialog
import kotlinx.android.synthetic.main.approval_dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApprovalDialog(context: Context?, val reservation: ReservationModel) : Dialog(context), Callback<ResponseModel<ReservationModel>> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.approval_dialog)
        setCancelable(false)
        this.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        buttonCancel.setOnClickListener {
            dismiss()
        }

        buttonSubmit.setOnClickListener {
            if (inputApprovalNote.text.isNotEmpty()) {
                if (radioApprove.isChecked) {
                    apiService.approveReservation(Employee.token(context)!!,
                            inputApprovalNote.text.toString(),
                            reservation.id
                    ).enqueue(this)
                }
                if (radioReject.isChecked) {
                    apiService.rejectReservation(Employee.token(context)!!,
                            inputApprovalNote.text.toString(),
                            reservation.id
                    ).enqueue(this)
                }
            } else {
                showAlertDialog(context, "", "Approval note must be filled.")
            }
        }
    }

    override fun onFailure(call: Call<ResponseModel<ReservationModel>>?, t: Throwable) {
        showAlertDialog(context, "", t.localizedMessage)
    }

    override fun onResponse(call: Call<ResponseModel<ReservationModel>>?, response: Response<ResponseModel<ReservationModel>>) {
        if (response.body()?.status == "200") {
            showAlertDialog(context, "", response.body()?.message!!)
            dismiss()
        } else {
            showAlertDialog(context, "", response.body()?.message!!)
        }
    }

}