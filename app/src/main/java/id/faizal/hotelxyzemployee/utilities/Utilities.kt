package id.faizal.hotelxyzemployee.utilities

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import com.google.gson.Gson
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

val sharedPreferencesKey = "HotelXYZSharedPreferences"

fun showAlertDialog(fromContext: Context, title: String, message: String) {
    AlertDialog.Builder(fromContext)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK", null)
            .create()
            .show()
}

fun showSnackbar(view: View, message: String) {
    Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    Log.d("snackbar", message)
}

fun showDatePicker(fromContext: Context, calendar: Calendar, listener: DatePickerDialog.OnDateSetListener) {
    DatePickerDialog(fromContext, listener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
            .show()
}

fun showTimePicker(fromContext: Context, calendar: Calendar, listener: TimePickerDialog.OnTimeSetListener) {
    TimePickerDialog(fromContext, listener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false)
            .show()
}

fun formatCalendar(calendar: Calendar, format: String = "MMM, d yyyy") : String {
    val date = calendar.time
    return SimpleDateFormat(format, Locale.UK).format(date)
}

fun toCalendar(str: String, format: String = "yyyy-MM-dd HH:mm:ss") : Calendar {
    val cal = Calendar.getInstance()
    cal.time = SimpleDateFormat(format, Locale.UK).parse(str)
    return cal
}

fun formatCurrency(value: Double) : String {
    val decimalFormat = DecimalFormat("#,###")
    val rate = "IDR " + decimalFormat.format(value* 1000)
    return rate.removeSuffix(".0")
}

fun printLog(o: Any) {
    Log.i("log", Gson().toJson(o))
}

