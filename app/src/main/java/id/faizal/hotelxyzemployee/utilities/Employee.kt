package id.faizal.hotelxyzemployee.utilities

import android.content.Context
import id.faizal.hotelxyzemployee.model.EmployeeModel
import id.faizal.hotelxyzemployee.model.ResponseModel
import id.faizal.hotelxyzemployee.model.TokenModel
import id.faizal.hotelxyzemployee.network.apiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object Employee : Callback<ResponseModel<EmployeeModel>> {

    var profile: EmployeeModel? = null
    var listener: GetProfileInterface? = null

    fun token(fromContext: Context) : String? {
        val sharedPreferences = fromContext.getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE)
        return sharedPreferences.getString("token", null)
    }

    fun hasToken(fromContext: Context) : Boolean {
        if (token(fromContext) != null) {
            return true
        }
        return false
    }

    fun refreshProfile(token: String, listener: GetProfileInterface) {
        this.listener = listener
        refreshProfile(token)
    }

    fun hasBeenLoggedIn(fromContext: Context) : Boolean {
        if (token(fromContext) != null && profile != null) {
            return true
        }
        return false
    }

    fun login(fromContext: Context, withToken: TokenModel, listener: GetProfileInterface) {
        val editor = fromContext.getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE).edit()
        editor.putString("token", withToken.token)
        editor.apply()

        refreshProfile(withToken.token, listener)
    }

    fun logout(fromContext: Context) {
        val editor = fromContext.getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE).edit()
        editor.remove("token")
        editor.apply()

        profile = null
    }

    private fun refreshProfile(token: String) {
        apiService.employeeProfile(token).enqueue(this)
    }

    override fun onFailure(call: Call<ResponseModel<EmployeeModel>>?, t: Throwable) {
        listener?.profileFailedToRetrievedBecause(t.localizedMessage)
        listener = null
        printLog(t)
    }

    override fun onResponse(call: Call<ResponseModel<EmployeeModel>>?, response: Response<ResponseModel<EmployeeModel>>?) {
        if (response?.body()?.status == "200") {
            profile = response.body()?.data
            listener?.profileSuccessRetrieved()
        } else {
            listener?.profileFailedToRetrievedBecause(response?.body()?.message!!)
        }
        listener = null
    }

    interface GetProfileInterface {
        fun profileSuccessRetrieved()
        fun profileFailedToRetrievedBecause(message: String)
    }

}