package id.faizal.hotelxyzemployee.network

import id.faizal.hotelxyzemployee.model.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

val serviceDateFormatPattern = "yyyy-MM-dd"

val baseURL: String = "http://faizal-hotel-xyz.96.lt/api/"

val retrofit: Retrofit =
        Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()


val apiService: ApiService = retrofit.create(ApiService::class.java)

interface ApiService {

    @POST("employee/profile")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun employeeProfile(

            @Header("Authorization")
            token: String

    ) : Call<ResponseModel<EmployeeModel>>


    @POST("employee/login")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun employeeLogin(

            @Field("email")
            email: String,

            @Field("password")
            password: String

    ) : Call<ResponseModel<TokenModel>>


    @POST("employee/reservation/list")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listPendingReservation(

            @Header("Authorization")
            token: String

    ) : Call<ResponseModel<PaginationModel<ReservationModel>>>

    @POST("employee/reservation/list/approved")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listApprovedReservation(

            @Header("Authorization")
            token: String

    ) : Call<ResponseModel<PaginationModel<ReservationModel>>>

    @POST("employee/reservation/list/rejected")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listRejectedReservation(

            @Header("Authorization")
            token: String

    ) : Call<ResponseModel<PaginationModel<ReservationModel>>>

    @POST("employee/reservation/approve")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun approveReservation(

            @Header("Authorization")
            token: String,

            @Field("note")
            note: String,

            @Field("reservation_id")
            reservationId: String

    ) : Call<ResponseModel<ReservationModel>>

    @POST("employee/reservation/reject")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun rejectReservation(

            @Header("Authorization")
            token: String,

            @Field("note")
            note: String,

            @Field("reservation_id")
            reservationId: String

    ) : Call<ResponseModel<ReservationModel>>

    @POST("employee/reservation/list-by-date")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listByDate(

            @Header("Authorization")
            token: String,

            @Field("field")
            field: String,

            @Field("from")
            from: String,

            @Field("to")
            to: String

    ) : Call<ResponseModel<PaginationModel<ReservationModel>>>

}