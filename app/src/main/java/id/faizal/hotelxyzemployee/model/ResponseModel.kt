package id.faizal.hotelxyzemployee.model

import com.google.gson.annotations.SerializedName

class ResponseModel<T>(

        @SerializedName("status")
        val status: String,

        @SerializedName("message")
        val message: String,

        @SerializedName("data")
        val data: T
)