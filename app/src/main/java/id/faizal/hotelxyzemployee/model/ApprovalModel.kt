package id.faizal.hotelxyzemployee.model

import com.google.gson.annotations.SerializedName

data class ApprovalModel (

        @SerializedName("created_by")
        val createdBy: Int,

        @SerializedName("status")
        val statusInt: Int,

        @SerializedName("note")
        val note: String

) {

    fun status(): ApprovalStatus {
        when(statusInt) {
            0 -> return ApprovalStatus.Rejected
            1 -> return ApprovalStatus.Approved
        }
        return ApprovalStatus.Undefined
    }

}

enum class ApprovalStatus {
    Rejected, Approved, Undefined
}