package id.faizal.hotelxyzemployee.model

data class TokenModel (
        val token: String
)