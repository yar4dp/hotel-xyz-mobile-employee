package id.faizal.hotelxyzemployee.model

import com.google.gson.annotations.SerializedName

data class PaginationModel<T> (

        @SerializedName("current_page")
        val currentPage: Int,

        @SerializedName("last_page")
        val lastPage: Int,

        @SerializedName("total")
        val total: Int,

        @SerializedName("data")
        val data: List<T>

)