package id.faizal.hotelxyzemployee.model

import com.google.gson.annotations.SerializedName
import id.faizal.hotelxyzemployee.utilities.formatCalendar
import id.faizal.hotelxyzemployee.utilities.toCalendar

data class ReservationModel(

        @SerializedName("id")
        val id: String,

        @SerializedName("created_by")
        val createdBy: CustomerModel,

        @SerializedName("room")
        val room: RoomModel,

        @SerializedName("approval")
        val approval: ApprovalModel,

        @SerializedName("total_person")
        val totalPerson: String,

        @SerializedName("check_in")
        val checkIn: String,

        @SerializedName("check_out")
        val checkOut: String,

        @SerializedName("estimated_rate")
        val estimatedRate: Double,

        @SerializedName("additional_request")
        val additionalRequest: String,

        @SerializedName("guest_first_name")
        val guestFirstName: String,

        @SerializedName("guest_last_name")
        val guestLastName: String,

        @SerializedName("guest_address")
        val guestAddress: String,

        @SerializedName("guest_phone_number")
        val guestPhoneNumber: String
) {

    fun guestFullName() : String = String.format("%s %s", guestFirstName, guestLastName)

    fun checkInTime() : String = formatCalendar(toCalendar(checkIn),"MMM, d yyyy hh:mm a")

    fun checkOutTime() : String = formatCalendar(toCalendar(checkOut), "MMM, d yyyy hh:mm a")

    fun checkInDate() : String = formatCalendar(toCalendar(checkIn), "MMM, d yyyy")

    fun checkOutDate() : String = formatCalendar(toCalendar(checkOut), "MMM, d yyyy")

}